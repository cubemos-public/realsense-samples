#!/usr/bin/env bash
if [ ! -d ./build ]; then
 mkdir build
fi

cd build && cmake -GNinja -DCMAKE_BUILD_TYPE=Release -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ../
ninja

cd ..
if [ ! -f ./compile_commands.json ]; then
    ln -s build/compile_commands.json .
fi
