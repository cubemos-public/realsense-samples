#include "realsense.hpp"
#include "utils.hpp"
#include <exception>
#include <librealsense2/h/rs_option.h>
#include <librealsense2/h/rs_sensor.h>
#include <thread>

#include <iostream>

namespace framos {

LowLevelDeviceStreamHandler::LowLevelDeviceStreamHandler(rs2::device const& device,
                                                         rs2_rs400_visual_preset preset)
  : _depth_queue(1)
  , _color_queue(1)
  , _dev(device)
  , _color_frame_counter(0)
  , _depth_frame_counter(0)
  , _depth_to_disparity_filter(true)
  , _disparity_to_depth_filter(false)
{
    _ir_frame_counter[0] = 0;
    _ir_frame_counter[1] = 0;
    _dec_filter.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2.0f);
    _spat_filter.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2.0f);
    _spat_filter.set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, 0.5f);
    _spat_filter.set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 20.0f);

    _temp_filter.set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, 0.4f);
    _spat_filter.set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 20.0f);

    _sensors = _dev.query_sensors();
    int color_index = 1;
    int depth_index = 0;

    std::vector<rs2::stream_profile> rgb_profiles(_sensors[1].get_stream_profiles());
    auto profile_iter =
      std::find_if(rgb_profiles.begin(), rgb_profiles.end(), [](rs2::stream_profile const& profile) {
          auto video_profile = profile.as<rs2::video_stream_profile>();
          return video_profile.fps() == 6 && video_profile.format() == RS2_FORMAT_BGR8 &&
                 video_profile.stream_type() == RS2_STREAM_COLOR && video_profile.width() == 1920 &&
                 video_profile.height() == 1080;
      });

    auto color_profile = profile_iter->as<rs2::video_stream_profile>();
    color_intrinsics = color_profile.get_intrinsics();
    _sensors[color_index].open(color_profile);

    std::vector<rs2::stream_profile> depth_profiles(_sensors[0].get_stream_profiles());
    profile_iter =
      std::find_if(depth_profiles.begin(), depth_profiles.end(), [](rs2::stream_profile const& profile) {
          auto video_profile = profile.as<rs2::video_stream_profile>();
          return video_profile.fps() == 6 && video_profile.format() == RS2_FORMAT_Z16 &&
                 video_profile.stream_type() == RS2_STREAM_DEPTH && video_profile.width() == 1280 &&
                 video_profile.height() == 720;
      });

    depth_to_color = profile_iter->as<rs2::video_stream_profile>().get_extrinsics_to(color_profile);
    depth_intrinsics = profile_iter->as<rs2::video_stream_profile>().get_intrinsics();
    std::vector<rs2::stream_profile> stereo_sensor_profiles;
    stereo_sensor_profiles.push_back(*profile_iter);
    set_sensor_param(_sensors[depth_index], RS2_OPTION_LASER_POWER, 300.0f);
    set_sensor_param(_sensors[depth_index], RS2_OPTION_VISUAL_PRESET, preset);

    std::vector<rs2::stream_profile>::iterator ir_profile_iters[2];
    for (int k = 0; k < 2; k++) {
        ir_profile_iters[k] = std::find_if(
          depth_profiles.begin(), depth_profiles.end(), [&k](rs2::stream_profile const& profile) {
              auto video_profile = profile.as<rs2::video_stream_profile>();
              return video_profile.fps() == 6 && video_profile.format() == RS2_FORMAT_Y8 &&
                     video_profile.stream_type() == RS2_STREAM_INFRARED && video_profile.width() == 1280 &&
                     video_profile.height() == 720 && video_profile.stream_index() == k + 1;
          });
        if (ir_profile_iters[k] == depth_profiles.end()) {
            throw std::runtime_error("Profile is not available");
        }
        stereo_sensor_profiles.push_back(*(ir_profile_iters[k]));
        ir_intrinsics[k] = ir_profile_iters[k]->as<rs2::video_stream_profile>().get_intrinsics();
    }

    _sensors[depth_index].open(stereo_sensor_profiles);

    auto ir_0_to_depth_extrinics = ir_profile_iters[0]->get_extrinsics_to(*profile_iter);
    if (ir_0_to_depth_extrinics.translation[0] == 0) {
        _ir_master_index = 0;
        _ir_slave_index = 1;
    }
    else {
        _ir_master_index = 1;
        _ir_slave_index = 0;
    }

    ir_slave_to_ir_master_extrinsics =
      ir_profile_iters[_ir_slave_index]->get_extrinsics_to(*ir_profile_iters[_ir_master_index]);

    device_serial_number = _dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
    std::stringstream s;
    s << std::this_thread::get_id();

    using namespace std::placeholders;

    auto color_callback = std::bind(&LowLevelDeviceStreamHandler::color_frame_callback, this, _1);
    _sensors[1].start(color_callback);

    auto depth_callback = std::bind(&LowLevelDeviceStreamHandler::depth_frame_callback, this, _1);
    _sensors[0].start(depth_callback);
}

void
LowLevelDeviceStreamHandler::color_frame_callback(rs2::frame frame)
{
    _color_frame_counter++;
    _color_queue.enqueue(frame);
    if ((_color_frame_counter % 10) == 0) {
        std::stringstream s;
        s << std::this_thread::get_id();
    }
}

rs2::frame
LowLevelDeviceStreamHandler::process_depth_frame(rs2::frame f)
{
    f = _dec_filter.process(f);
    f = _depth_to_disparity_filter.process(f);
    f = _spat_filter.process(f);
    f = _temp_filter.process(f);
    f = _disparity_to_depth_filter.process(f);
    return f;
}

void
LowLevelDeviceStreamHandler::handle_frame_from_stereo_sensor(rs2::frame f)
{
    auto profile = f.get_profile();
    if (profile.stream_type() == rs2_stream::RS2_STREAM_DEPTH) {
        _depth_queue.enqueue(process_depth_frame(f));
        _depth_frame_counter++;
        if ((_depth_frame_counter % 10) == 0) {
            std::stringstream s;
            s << std::this_thread::get_id();
        }
    }
    else if (profile.stream_type() == rs2_stream::RS2_STREAM_INFRARED) {
        int index = profile.stream_index() - 1;
        _ir_frame_counter[index]++;
        _ir_queue[index].enqueue(f);

        if ((_ir_frame_counter[1] % 10) == 0) {
            std::stringstream s;
            s << std::this_thread::get_id();
        }
    }
}

void
LowLevelDeviceStreamHandler::depth_frame_callback(rs2::frame frame)
{
    _depth_frame_counter++;
    if (rs2::frameset fs = frame.as<rs2::frameset>()) {
        for (rs2::frame f : fs) {
            handle_frame_from_stereo_sensor(f);
        }
    }
    else {
        handle_frame_from_stereo_sensor(frame);
    }
}

LowLevelDeviceStreamHandler::~LowLevelDeviceStreamHandler()
{
    for (auto sensor : _sensors) {
        try {
            sensor.stop();
            sensor.close();
        }
        catch (rs2::error& err) {
            std::cerr << err.what() << std::endl;
        }
    }
    _dev.hardware_reset();
}

void
MultiDeviceWrapper::start(rs2_rs400_visual_preset preset)
{
    rs2::context ctx;
    auto device_list = ctx.query_devices();
    _device_callbacks.clear();
    _device_callbacks.reserve(device_list.size());

    for (auto&& dev : device_list) {
        if (dev.supports(rs2_camera_info::RS2_CAMERA_INFO_SERIAL_NUMBER)) {
            _device_callbacks.emplace_back(std::make_unique<LowLevelDeviceStreamHandler>(dev, preset));
        }
    }
}

std::vector<std::string>
MultiDeviceWrapper::device_licenses() const
{
    std::vector<std::string> result;
    for (auto&& handler : _device_callbacks) {
        result.push_back(handler->device_serial_number);
    }
    return result;
}

void
MultiDeviceWrapper::stop()
{
    _device_callbacks.clear();
}

void
MultiDeviceWrapper::getRecentColorFrames(std::map<std::string, rs2::frame>& frames)
{
    for (auto&& handler : _device_callbacks) {
        std::string serialNumber = handler->device_serial_number;
        try {
            auto frame = handler->_color_queue.wait_for_frame();
            frames[serialNumber] = frame;
        }
        catch (std::exception const&) {
            std::cerr << "Exception thrown while waiting for color frame of camera: " << serialNumber
                      << std::endl;
            throw;
        }
    }
}

void
MultiDeviceWrapper::getRecentDepthFrames(std::map<std::string, rs2::frame>& frames)
{
    for (auto&& handler : _device_callbacks) {
        std::string serialNumber = handler->device_serial_number;
        try {
            auto frame = handler->_depth_queue.wait_for_frame();
            frames[serialNumber] = frame;
        }
        catch (std::exception const&) {
            std::cerr << "Exception thrown while waiting for depth frame of camera: " << serialNumber
                      << std::endl;
            throw;
        }
    }
}

void
MultiDeviceWrapper::getRecentIrFrames(std::map<std::string, std::map<std::string, rs2::frame>>& frames)
{
    for (auto&& handler : _device_callbacks) {
        std::string serialNumber = handler->device_serial_number;
        try {

            {
                auto frame = handler->_ir_queue[handler->_ir_master_index].wait_for_frame();
                frames[serialNumber]["master"] = frame;
            }
            {
                auto frame = handler->_ir_queue[handler->_ir_slave_index].wait_for_frame();
                frames[serialNumber]["slave"] = frame;
            }
        }
        catch (std::exception const&) {
            std::cerr << "Exception thrown while waiting for ir frame-pair of camera: " << serialNumber
                      << std::endl;
            throw;
        }
    }
}

std::map<std::string, std::map<std::string, rs2_intrinsics>>
MultiDeviceWrapper::getIrIntrinsics()
{

    std::map<std::string, std::map<std::string, rs2_intrinsics>> result;
    for (auto&& handler : _device_callbacks) {
        result[handler->device_serial_number]["master"] = handler->ir_intrinsics[handler->_ir_master_index];
        result[handler->device_serial_number]["slave"] = handler->ir_intrinsics[handler->_ir_slave_index];
    }
    return result;
}

std::map<std::string, rs2_extrinsics>
MultiDeviceWrapper::getIrSlaveToIrMasterExtrinsics() const
{
    std::map<std::string, rs2_extrinsics> result;
    for (auto&& handler : _device_callbacks) {
        result[handler->device_serial_number] = handler->ir_slave_to_ir_master_extrinsics;
    }
    return result;
}

}
