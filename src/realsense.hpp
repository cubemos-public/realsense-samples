#pragma once
#include <librealsense2/h/rs_option.h>
#include <librealsense2/h/rs_types.h>
#include <librealsense2/hpp/rs_types.hpp>
#include <librealsense2/rs.hpp>

#include <map>
#include <vector>
namespace framos {

class LowLevelDeviceStreamHandler {
  public:
    LowLevelDeviceStreamHandler(rs2::device const& dev, rs2_rs400_visual_preset preset);
    ~LowLevelDeviceStreamHandler();
    void color_frame_callback(rs2::frame frame);
    void depth_frame_callback(rs2::frame frame);
    rs2::frame process_depth_frame(rs2::frame frame);
    void handle_frame_from_stereo_sensor(rs2::frame frame);

    std::string device_serial_number;
    rs2::frame_queue _depth_queue;
    rs2::frame_queue _color_queue;
    rs2::frame_queue _ir_queue[2];
    rs2_intrinsics ir_intrinsics[2];
    int _ir_slave_index;
    int _ir_master_index;
    rs2_intrinsics color_intrinsics;
    rs2_intrinsics depth_intrinsics;
    rs2_extrinsics depth_to_color;
    rs2_extrinsics ir_slave_to_ir_master_extrinsics;

  private:
    rs2::device _dev;
    std::vector<rs2::sensor> _sensors;
    int _color_frame_counter;
    int _ir_frame_counter[2];
    int _depth_frame_counter;

    // filter
    rs2::decimation_filter _dec_filter;
    rs2::threshold_filter _thres_filter;
    rs2::disparity_transform _depth_to_disparity_filter;
    rs2::disparity_transform _disparity_to_depth_filter;
    rs2::spatial_filter _spat_filter;
    rs2::temporal_filter _temp_filter;
};

class MultiDeviceWrapper {
  public:
    void start(rs2_rs400_visual_preset preset);
    std::vector<std::string> device_licenses() const;
    void stop();
    void resetHardware();
    void getRecentColorFrames(std::map<std::string, rs2::frame>& frames);
    void getRecentDepthFrames(std::map<std::string, rs2::frame>& frames);
    void getRecentIrFrames(std::map<std::string, std::map<std::string, rs2::frame>>& frames);
    std::map<std::string, rs2_intrinsics> getIntrinsics(rs2_stream streamType, int index = 0);
    std::map<std::string, std::map<std::string, rs2_intrinsics>> getIrIntrinsics();
    std::map<std::string, rs2_extrinsics> getDepthToColorExtrinsics() const;
    std::map<std::string, rs2_extrinsics> getIrSlaveToIrMasterExtrinsics() const;

  public:
    std::vector<std::unique_ptr<LowLevelDeviceStreamHandler>> _device_callbacks;
};
}
