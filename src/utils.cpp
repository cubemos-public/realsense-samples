#include "utils.hpp"

#include <iomanip>
#include <iostream>
#include <sstream>
namespace framos {

rs2_intrinsics
frame_intrinsics(rs2::frame const& frame)
{
    return frame.get_profile().as<rs2::video_stream_profile>().get_intrinsics();
}

void
set_sensor_param(rs2::sensor& sensor, rs2_option option, float value)
{
    try {
        sensor.set_option(option, value);
    }
    catch (const rs2::error& e) {
        std::ostringstream os;
        os << "Failed to set sensor option" << rs2_option_to_string(option) << ". Details: " << e.what()
           << std::endl;
        throw std::runtime_error(os.str());
    }
}

cv::Mat
wrapFrame(rs2::frame input, bool copyData)
{
    const int w = input.as<rs2::video_frame>().get_width();
    const int h = input.as<rs2::video_frame>().get_height();
    int bytes_per_pixel = input.as<rs2::video_frame>().get_bytes_per_pixel();
    int stride = input.as<rs2::video_frame>().get_stride_in_bytes();
    int datatype = -1;
    if (input.is<rs2::depth_frame>()) {
        datatype = CV_16UC1;
    }
    else {
        datatype = CV_MAKETYPE(CV_8U, bytes_per_pixel);
    }
    cv::Mat wrapperImg(cv::Size(w, h),
                       datatype,
                       const_cast<void*>(static_cast<void const*>(input.get_data())),
                       static_cast<size_t>(stride));
    if (copyData) {
        return wrapperImg.clone();
    }
    else {
        return wrapperImg;
    }
}

std::ostringstream
get_current_timestamp()
{
    using clock = std::chrono::system_clock;
    auto now = clock::now();
    auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
    std::time_t t_now = clock::to_time_t(clock::now());
    std::ostringstream os;
    os << std::put_time(std::localtime(&t_now), "%F_%H-%M-%S");
    os << std::setfill('0') << std::setw(3) << ms.count() % 1000;
    return os;
}

}
