#include "realsense.hpp"
#include "utils.hpp"

#include <chrono>
#include <iostream>
#include <ostream>
#include <sstream>
#include <string>
#include <thread>

#include <librealsense2/h/rs_option.h>
#include <librealsense2/hpp/rs_processing.hpp>

#include <opencv2/imgcodecs.hpp>

int
main(int argc, char* argv[])
try {
    int preset_option = RS2_RS400_VISUAL_PRESET_HIGH_ACCURACY;
    if (argc == 2) {
        preset_option = std::stoi(argv[1]);
        if (preset_option <= 0 || preset_option >= RS2_RS400_VISUAL_PRESET_COUNT) {
            std::cerr << "Invalid preset " << preset_option << std::endl;
            return 1;
        }
    }
    rs2::colorizer colorizer;
    rs2_rs400_visual_preset preset = static_cast<rs2_rs400_visual_preset>(preset_option);
    framos::MultiDeviceWrapper wrapper;
    wrapper.start(preset);
    std::this_thread::sleep_for(std::chrono::seconds(5));
    std::map<std::string, rs2::frame> frames;
    wrapper.getRecentDepthFrames(frames);
    for (auto&& p : frames) {
        std::ostringstream filename;
        filename << p.first << "_" << rs2_rs400_visual_preset_to_string(preset) << ".png";

        auto colorized_depth = colorizer.colorize(p.second);
        auto col_mat = framos::wrapFrame(colorized_depth);
        cv::imwrite(filename.str(), col_mat);
    }
}
catch (std::exception& e) {
    std::cerr << "exception occured: " << e.what() << std::endl;
    return -1;
}
catch (...) {
    std::cerr << "unhandled exception occured" << std::endl;
    return -1;
}
