#pragma once
#include <librealsense2/rs.hpp>
#include <opencv2/core.hpp>
#include <string>

namespace framos {

rs2_intrinsics
frame_intrinsics(rs2::frame const& frame);

void
set_sensor_param(rs2::sensor& sensor, rs2_option option, float value);

cv::Mat
wrapFrame(rs2::frame input, bool copyData = false);

std::ostringstream
get_current_timestamp();

} // namespace framos
